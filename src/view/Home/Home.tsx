import React from 'react'
import List from '../../components/List/List'
import Search from '../../components/Search/Search'

function Home() {
    return (
        <div>
            <Search></Search>
            <List></List>
        </div>
    )
}

export default Home

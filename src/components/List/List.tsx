import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { initData } from '../../data/data.json'
import ListItem from './ListItem'
function List() {
    const [data, setData] = useState<any>(initData)
    return (
        <Warp>
            {initData.map((item) => {
                return <ListItem key={item.id}
                    id={item.id}
                    name={item.name}
                    status={item.status}
                />
            })}
        </Warp>
    )
}

export default List
const Warp = styled.div`
    width: 50%;
    padding:0 20px 20px;
    margin: 0 auto;
`
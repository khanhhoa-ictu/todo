import React, { useState } from 'react'
import styled from 'styled-components'
interface LisItemProps {
    id: number,
    name: string,
    status: boolean
}
function ListItem({ name, status }: LisItemProps) {
    const [openModalEdit, setOpenModalEdit] = useState(false)
    const [openModalDelete, setOpenModalDelete] = useState(false)
    const handleClickModalEdit = () => {
        setOpenModalEdit(true)
    }
    const handleClickModalDelete = () => {
        setOpenModalDelete(true)
    }
    return (
        <Warp>
            <div className="li__name"> {name}</div>
            <div className="li__button">
                <button onClick={()=>handleClickModalEdit() } className="li__button__edit" >Edit</button>
                <button onClick={()=>handleClickModalDelete() } className="li__button__delete">Delete</button>
                <button className="li__button__complate">Complate</button>
            </div>
        </Warp>
    )
}

export default ListItem
const Warp = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 20px;
    border: 1px solid #e9e9e9;
    border-radius: 5px;
    margin-bottom: 3px;
    box-shadow: 0px 0px 2px #e9e9e9;
    .li__name{
        flex:1;
    }
    .li__button{
        button{
            outline: none;
            border: none;
            padding: 10px;
            margin: 5px;
            width: 80px;
            border-radius: 5px;
            color: white;

        }
        &__edit{
            background-color:#30d9d6;
        }
        &__delete{
            background-color: #d10f29;
        }
        &__complate{
            background-color: #bab9af;
        }
    }
`

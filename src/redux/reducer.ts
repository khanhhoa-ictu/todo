import { combineReducers } from 'redux';
import { HomeReducer, HomeState } from '../view/Home/reducer';
const allReducers = combineReducers({
    HomeReducer,
});
const rootReducer = (state: any, action: any) => {
    if (action.type === "TEST") {
        state = {}
    }
    return allReducers(state, action);
};
export type ApplicationState = ReturnType<typeof rootReducer>;
export default rootReducer